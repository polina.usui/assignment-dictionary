lib.o: lib.inc
	nasm -f elf64 -o lib.o lib.inc

dict.o: dict.asm lib.o
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm dict.o words.inc
	nasm -f elf64 -o main.o main.asm

lab2: main.o dict.o lib.o
	ld -o lab2 dict.o main.o lib.o

.PHONY: clean
clean:
	rm -rf *.o
	rm -rf lab2
