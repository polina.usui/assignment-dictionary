global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy	

section .text

exit: 
    mov     rdi, rax
    mov     rax, 60        
    syscall

string_length:
    xor rax, rax            
	.loop:
		cmp byte[rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret   

print_string:
	call string_length
	
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
	ret	

print_char:
    push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdx, 1
	mov rdi, 1
	syscall
	pop rdi
	ret

print_newline:
    mov rdi, 0xA
	jmp print_char

print_uint:
    mov rax, rdi
    mov rdi, 10		
    mov rsi, rsp            
    dec rsp             
    mov [rsp], byte 0
	
	.loop:		
		xor rdx, rdx
		div rdi
		add rdx, '0'
		dec rsp
		mov [rsp], dl
		test rax, rax
		jnz .loop
		
	.end:
		mov rdi, rsp
		push rsi
		call print_string
		pop rsp
		ret

print_int:
    test rdi,rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint



string_equals:
    .for_each:
		mov al, [rsi]
		cmp al, [rdi]
		jne .not_equal
		inc rdi
		inc rsi
		test al, al
		jnz .for_each
		mov eax, 1
		ret
	.not_equal:
		xor rax, rax
		ret


read_char:
	mov rax, 0
	mov rdi, 0
	dec rsp
    mov [rsp], al
    mov rsi, rsp
    mov rdx,1
    syscall
    mov al, [rsp]
    inc rsp
    ret 


read_word:
	.delete_spaces:			; delete all start spaces symb
		push rdi
		push rsi
		push rdx	
		call read_char
		pop rdx
		pop rsi
		pop rdi	

		cmp rax, 0x09
		je .delete_spaces
		cmp rax, 0x0A
		je .delete_spaces
		cmp rax, 0x20
		je .delete_spaces

	.before_loop:
		xor rdx, rdx			; 0 -> counter
	.read_char_loop:
		cmp rdx, rsi
		je .str_error

		cmp rax, 0x00			; checking the end of word
		je .str_ok
		cmp rax, 0x09
		je .str_ok
		cmp rax, 0x0A
		je .str_ok
		cmp rax, 0x20
		je .str_ok

		mov byte [rdi + rdx], al
		inc rdx

		push rdi
		push rsi
		push rdx
		call read_char
		pop rdx
		pop rsi
		pop rdi	

		jmp .read_char_loop

	.str_ok:
		mov byte [rdi + rdx], 0		; null-term
		mov rax, rdi			; returning adress of buf
		jmp .end
		.str_error:
		xor rax, rax			; 0 -> result
		jmp .end
	.end:	
		ret	


parse_uint:
    xor	rax,rax
	xor	rdx,rdx
    push rbx
	.loop:
		xor	rbx, 0
		mov	bl, byte[rdi+rdx]
		cmp	bl, '0'
		jb	.end
		cmp	bl, '9'
		ja	.end
		sub	bl, '0'              
		imul rax, 10
		add	rax, rbx
		inc	rdx
		jnz	.loop
	.end:
		pop	rbx
		ret

parse_int:
	xor rdx, rdx
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
    .minus:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
    .end:
        ret


string_copy:

    push rdx
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    pop rdx

    xor r9, r9
    inc rax
    cmp rdx, rax
    jl .buffer_overflow
  
  .loop:
    mov cl, byte[rdi + r9]
    mov byte[rsi + r9], cl
    inc r9
    cmp r9, rax
    jl .loop
  .end:
    dec rax
    ret
  .buffer_overflow:
    mov rax, 0
    ret
