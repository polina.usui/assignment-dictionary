%include "words.inc"
%define SIZE 255

extern find_word
extern read_word
extern string_length
extern exit
extern print_string

global _start

section .data
buffer: times SIZE db 0

section .rodata
er_msg: db "Your string ys too long. Try again.", 0
er_key: db "The key does not exist in Dictionary", 0

section .text

_start:
	mov rdi, buffer
	mov rsi, SIZE
 	call read_word

 	test rax, rax
 	je .msg_error
 	mov rdi, rax

 	mov rsi, word_1
 	push rdx
 	call find_word
 	pop rdx
 	test rax, rax
 	je .key_error
 	.success:
  		mov rdi, rax
  		add rdi, 8
  		add rdi, rdx
		add rdi, 1
  		call print_string
  		call exit

	.msg_error:
		mov rdi, er_msg
		jmp .end

 	.key_error:
 		mov rdi, er_key
  		jmp .end

 	.end:
	 	call print_error
		mov rax, 1
		call exit


	print_error:
  		mov rsi, rdi
  		call string_length
		mov rdx, rax; длина выводимого сообщения (без нуль-терминатора)
		mov rdi, 2; inode файла в который нужно вывести строку (в нашем случае stderr)
		mov rax, 1; номер системного вызова (sys_write)
  		syscall
  		ret
