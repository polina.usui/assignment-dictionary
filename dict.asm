extern string_equals

global find_word

section .text

find_word: ; rdi - key, rsi - start of map
    .loop:
     ;mov r8, rsi
     ;mov r9, rdi
     add rsi, 8

     push rsi
     push rdi
     call string_equals
     pop rdi
     pop rsi

     cmp rax, 0
     jne .found
     sub rsi, 8
     mov rsi, [rsi]
     test rsi, rsi
     je .not_found
     jmp .loop

    .found:
     sub rsi, 8
     mov rax, rsi
     ret

    .not_found:
     ;pop rdi
     ;pop rsi
     xor rax, rax
     ret
